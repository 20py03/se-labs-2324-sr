from query_handler_base import QueryHandlerBase
import requests
import json

class bmi(QueryHandlerBase):
    def can_process_query(self, query):
        if "bmi" in query:
            return True
        return False

    def process(self, query):
        bmi = query.split()
        height = bmi[1]
        weight = bmi[2]

        try:
            result = self.call_api(height, weight)
            score = result["BMI"]
            score2 = result["Class"]
            self.ui.say(f"Your BMI is: {score} and you are: {score2}")
        except: 
            self.ui.say("Oh no! Your input is wrong")
            self.ui.say("Try again!")



    def call_api(self, height, weight):
        url = "https://bmi-calculator6.p.rapidapi.com/bmi"

        querystring = {"height":height,"weight":weight,"system":"metric"}

        headers = {
	        "X-RapidAPI-Key": "9edba59626mshb2357dab105b243p1cb13cjsn9744eab88faf",
	        "X-RapidAPI-Host": "bmi-calculator6.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers, params=querystring)

        return json.loads(response.text)
