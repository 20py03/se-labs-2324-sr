import csv
from csv import DictReader


input_file="ex2-text.csv"
employee_file="ex2-employees.txt"
location_file="ex2-locations.txt"

employees=[]

with open(input_file, 'r') as file:
    dict_reader=DictReader(file)
    employees=list(dict_reader)
    print(employees)

file.close()

    