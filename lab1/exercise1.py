def count_evens(even_numbers):
    count=0
    for i in even_numbers:
        if i%2==0:
            count+=1
    return count
    
input_num=input ("Input numbers separated by coma:")
input_li=input_num.split(',')
int_list=[int(i) for i in input_li]

even=count_evens(int_list)

print("Number of even integers: ",even)