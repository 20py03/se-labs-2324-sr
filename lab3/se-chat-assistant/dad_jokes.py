from query_handler_base import QueryHandlerBase
import random
import requests
import json
import time

class Jokes(QueryHandlerBase):
    def can_process_query(self, query):
        if "dad" in query:
            return True
        return False

    def process(self, query):
        

        try:
            result = self.call_api()
            result = result["body"][0]
            setup = result["setup"]
            punchline=result["punchline"]
            self.ui.say(setup)
            time.sleep(2)
            self.ui.say(punchline)
        except: 
            self.ui.say("Oh no! There was an error trying to contact Love api.")
            self.ui.say("Try something else!")



    def call_api(self):

        url = "https://dad-jokes.p.rapidapi.com/random/joke"

        headers = {
	        "X-RapidAPI-Key": "9edba59626mshb2357dab105b243p1cb13cjsn9744eab88faf",
	        "X-RapidAPI-Host": "dad-jokes.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        return json.loads(response.text)
