import csv

input_file="ex2-text.csv"
employee_file="ex2-employees.txt"
location_file="ex2-locations.txt"

with open(input_file, 'r') as file:
    reader=csv.reader(file, delimiter=',')
    next(reader, None)
    for row in reader:
        with open(employee_file,'a') as emp_file:
            emp_file.write(f'{row[0]},{row[1]}\n')
        with open(location_file,'a') as loc_file:
            loc_file.write(f'{row[0]},{row[3]}\n')
            
file.close()
    






