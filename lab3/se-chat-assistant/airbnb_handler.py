from query_handler_base import QueryHandlerBase
import requests
import json

class Airbnb(QueryHandlerBase):
    def can_process_query(self, query):
        if "airbnb" in query:
            return True
        return False

    def process(self, query):
        airbnb = query.split()
        location = airbnb[1]
        check_in_date = airbnb[2]
        check_out_date=airbnb[3]
        number_of_adults=airbnb[4]

        try:
            result = self.call_api(location, check_in_date,check_out_date,number_of_adults)
            score = result["results"]
            score = score[0]["name"]
            score2 = result["results"]
            score2 = score2[0]
            score2 = score2["price"]["total"]
            self.ui.say(f"Results for your search is: {score} and the price is: {score2} USD")
        except: 
            self.ui.say("Oh no! There is no available accommodation for that date!")
            self.ui.say("Try something else!")



    def call_api(self, location, check_in_date,check_out_date,number_of_adults):
        url = "https://airbnb13.p.rapidapi.com/search-location"

        querystring = {"location":location,"checkin":check_in_date,"checkout":check_out_date,"adults":number_of_adults,"children":"0","infants":"0","pets":"0","page":"1","currency":"USD"}

        headers = {
	        "X-RapidAPI-Key": "9edba59626mshb2357dab105b243p1cb13cjsn9744eab88faf",
	        "X-RapidAPI-Host": "airbnb13.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers, params=querystring)

        return json.loads(response.text)
