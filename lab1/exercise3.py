def twos(numbers):
    for i in range(len(numbers)-1):
        if numbers[i]==2 and numbers[i+1]==2:
            return True
    return False
    
input_num=input ("Input numbers separated by coma:")
input_li=input_num.split(',')
int_list=[int(i) for i in input_li]

result=twos(int_list)

if(result):
    print("The list contains 2 next to a 2")
else:
    print("The list does not contain 2 next to a 2")