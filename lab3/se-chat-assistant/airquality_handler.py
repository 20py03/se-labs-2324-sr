from query_handler_base import QueryHandlerBase
import requests
import json

class Quality(QueryHandlerBase):
    def can_process_query(self, query):
        if "air-quality" in query:
            return True
        return False
    
    def process(self, query):
        city = query.split(" ")
        city= city[1]

        try:
            result = self.call_api(city)
            score = result["data"]
            score = score["cities"]
            score = score[0]
            score = score["currentMeasurement"]
            score = score["aqius"]
            self.ui.say(f"Quality of air in {city} is {score}")
        except: 
            self.ui.say("Oh no! In {city} is not possible to measure air-quality!")
            self.ui.say("Try something else!")


    def call_api(self, city):
        url = "https://airvisual1.p.rapidapi.com/v2/auto-complete"

        querystring = {"q":city,"x-user-lang":"en-US","x-aqi-index":"us","x-units-pressure":"mbar","x-units-distance":"kilometer","x-units-temperature":"celsius"}

        headers = {
	        "X-RapidAPI-Key": "9edba59626mshb2357dab105b243p1cb13cjsn9744eab88faf",
	        "X-RapidAPI-Host": "airvisual1.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers, params=querystring)

        return json.loads(response.text)
    