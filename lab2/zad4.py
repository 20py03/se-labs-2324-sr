import csv
from csv import DictReader
import json

input_file="ex2-text.csv"

employees=[]
with open(input_file, 'r') as file:
    dict_reader=DictReader(file)
    employees=list(dict_reader)
    with open('ex4-employees.json', 'w', encoding='utf-8') as f:
        json.dump(employees, f)

file.close()

    