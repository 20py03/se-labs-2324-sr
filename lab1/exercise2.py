def avg(numbers):
    sorted_numbers=sorted(numbers)
    trimmed_numbers=sorted_numbers[1:-1]

    total=sum(trimmed_numbers)
    count=len(trimmed_numbers)

    if count==0:
        return 0
    
    avg=total//count
    return  avg



input_num=input ("Input numbers separated by coma:")
input_li=input_num.split(',')
int_list=[int(i) for i in input_li]

if len(int_list)<3:
    print("The list must have at least 3 elements")
else:
    result=avg(int_list)
    print("Average : ",result)