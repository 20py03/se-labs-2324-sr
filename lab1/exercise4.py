def strings(s):
    uppercase=0
    lowercase=0
    digit=0

    for char in s:
        if char.isupper():
            uppercase+=1
        elif char.islower():
            lowercase+=1
        elif char.isdigit():
            digit+=1
    return (uppercase,lowercase,digit)

input_s=input("Enter a string: ")
counts=strings(input_s)

uppercase,lowercase,digit=counts
print(f"{uppercase,lowercase,digit}")

    